package com.training.burenkova.hw6.task1;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class InputTextParsingTest {
    InputTextParsing inputTextParsing = new InputTextParsing();

    @Before
    public void beforeTest() {
        inputTextParsing.splitByWords("ab, c;b,a, A");
    }

    @Test(expected = NullPointerException.class)
    public void displayBalanceInDifferentCurrencyException() {
        inputTextParsing.splitByWords(null);
    }

    @Test
    public void splitByWords() {

        List<String> expectedSortedList = new ArrayList<>();
        expectedSortedList.add("ab");
        expectedSortedList.add("c");
        expectedSortedList.add("b");
        expectedSortedList.add("a");
        expectedSortedList.add("A");
        List<String> sortedList = inputTextParsing.getWords();
        assertEquals(expectedSortedList, sortedList);
    }

    @Test
    public void groupByAlphabetList() {
        inputTextParsing.groupByAlphabetList();
        Map<String, Integer> expectedWordList = new HashMap<>();
        Set<String> expectedGroupList = new HashSet<>();
        expectedWordList.put("ab", 1);
        expectedWordList.put("c", 1);
        expectedWordList.put("b", 1);
        expectedWordList.put("a", 2);
        expectedGroupList.add("ab");
        expectedGroupList.add("c");
        expectedGroupList.add("b");
        expectedGroupList.add("a");
        Map<String, Integer> wordList = inputTextParsing.getWordList();
        Set<String> groupList = inputTextParsing.getGroupList();
        assertEquals(expectedWordList, wordList);
        assertEquals(expectedGroupList, groupList);

    }

    @Test
    public void fillFirstLetterList() {
        inputTextParsing.showGroupedList();
        inputTextParsing.fillFirstLetterList();
        Set<Character> expectedList = new TreeSet<>();
        expectedList.add('a');
        expectedList.add('b');
        expectedList.add('c');
        Set<Character> list = inputTextParsing.getFirstLetterList();
        assertEquals(expectedList, list);
    }


}