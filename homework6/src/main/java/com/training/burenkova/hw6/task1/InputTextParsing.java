package com.training.burenkova.hw6.task1;

import java.util.*;

/**
 * This class parses the incoming string and groups the words by first letter in alphabetical order
 */
public class InputTextParsing {
    private List<String> words;
    private Map<String, Integer> wordList;
    private Set<String> groupList;
    private Set<String> sortedList;
    private Set<Character> firstLetterList;

    public InputTextParsing() {
        words = new ArrayList<>();
    }

    /**
     * This method splits incoming string by words
     *
     * @param text - incoming string
     */
    public void splitByWords(String text) {
        words = Arrays.asList(text.split("\\s*(\\W)\\s*"));
        if (words == null) {
            throw new NullPointerException();
        }
    }

    /**
     * This method fills field firstLetterList, which contains a list of the first letters of words
     */
    public void fillFirstLetterList() {
        firstLetterList = new TreeSet<>();
        for (String word : sortedList) {
            firstLetterList.add(word.charAt(0));
        }
    }

    /**
     * This method shows grouped words by first letter in alphabetical order
     */
    public void showGroupedList() {
        groupByAlphabetList();
        sortedList = new TreeSet();
        sortedList.addAll(groupList);
        fillFirstLetterList();
        for (char firstLetter : firstLetterList) {
            System.out.println(firstLetter + ":");
            for (String word : sortedList) {
                if (firstLetter == word.charAt(0)) {
                    System.out.println((word + " count= " + wordList.get(word)));
                }
            }
        }
    }

    /**
     * This method groups words by alphabet
     */
    public void groupByAlphabetList() {
        wordList = new HashMap<>();
        groupList = new HashSet<>();
        Integer countOfDoublingWord;
        for (int i = 0; i < words.size(); i++) {
            countOfDoublingWord = wordList.get(words.get(i).toLowerCase());
            wordList.put(words.get(i).toLowerCase(), countOfDoublingWord == null ? 1 : countOfDoublingWord + 1);
            groupList.add(words.get(i).toLowerCase());
        }

    }

    public List<String> getWords() {
        return words;
    }

    public Set<Character> getFirstLetterList() {
        return firstLetterList;
    }

    public Map<String, Integer> getWordList() {
        return wordList;
    }

    public Set<String> getGroupList() {
        return groupList;
    }
}

