package com.training.burenkova.hw5.task1;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class InputDataParsingTest {

    @Test
    public void parse() {
        InputDataParsing inputDataParsing = new InputDataParsing();
        List<String> expectedFileModelList = new ArrayList<>();
        expectedFileModelList.add("a");
        expectedFileModelList.add("b");
        assertEquals(expectedFileModelList,inputDataParsing.parse("a/b"));
    }
}