package com.training.burenkova.hw5.task1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class FolderTest {
    Folder folder;

    @Before
    public void beforeTest() {
        folder = new Folder();
    }

    @After
    public void afterTest() {
        folder = null;
    }

    @Test
    public void addFile_ifTrue() {
        assertTrue(folder.addFile(new File("a.txt")));
    }

    @Test
    public void addFile_ifFalse() {
        File file = new File("a.txt");
        folder.addFile(file);
        assertFalse(folder.addFile(file));
    }

    @Test
    public void addFolder_ifTrue() {
        assertTrue(folder.addFolder(new File("a.txt")));
    }

    @Test
    public void addFolder_ifFalse() {
        Folder folder = new Folder("a");
        folder.addFile(folder);
        assertFalse(folder.addFolder(folder));
    }

}