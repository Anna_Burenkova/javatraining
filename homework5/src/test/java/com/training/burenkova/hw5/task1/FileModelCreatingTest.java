package com.training.burenkova.hw5.task1;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FileModelCreatingTest {

    @Test
    public void createFileModel() {
        FileModelCreating fileModelCreating = new FileModelCreating();
        Folder expectedModel = new Folder();
        Folder folderA = new Folder("a");
        folderA.addFolder(new Folder("b.txt"));
        expectedModel.addFolder(folderA);
        List<String> model = new ArrayList<>();
        model.add("a");
        model.add("b.txt");
        assertEquals(expectedModel, fileModelCreating.createFileModel(model));
    }
}