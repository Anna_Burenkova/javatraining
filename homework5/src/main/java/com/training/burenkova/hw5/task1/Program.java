package com.training.burenkova.hw5.task1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        boolean n = true;
        FileModelCreating fileModelCreating = new FileModelCreating();
        Folder folderStructure = new Folder();
        while (n) {
            InputDataParsing inputDataParsing = new InputDataParsing();
            System.out.println();
            System.out.println("Enter a path:");
            Scanner in = new Scanner(System.in);
            String filePath = in.nextLine();
            List<String> list = new ArrayList<>();
            for (String a : inputDataParsing.parse(filePath)) {
                list.add(a);
            }
            folderStructure = fileModelCreating.createFileModel(list);
            folderStructure.print("-");

        }
    }
}
