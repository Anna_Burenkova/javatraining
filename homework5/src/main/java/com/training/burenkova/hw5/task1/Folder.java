package com.training.burenkova.hw5.task1;

import java.util.HashSet;
import java.util.Objects;

public class Folder implements FileModel {
    private String folderName;
    private HashSet<FileModel> fileModelSet = new HashSet<>();

    public Folder() {
        folderName = "";
        fileModelSet = new HashSet<>();
    }

    public Folder(String folderName) {
        this.folderName = folderName;
    }

    public boolean addFile(FileModel file) {
        return fileModelSet.add(file);
    }

    public boolean addFolder(FileModel folder) {
        return fileModelSet.add(folder);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Folder folder = (Folder) o;
        return Objects.equals(folderName, folder.folderName);
    }


    @Override
    public int hashCode() {
        return Objects.hash(folderName);
    }

    @Override
    public void print(String tab) {
        String tabNext = tab + tab.charAt(0);
        System.out.println(tab + "/" + folderName);
        for (FileModel fileFolder : fileModelSet) {
            fileFolder.print(tabNext);
        }
    }

    public HashSet<FileModel> getFileModelSet() {
        return fileModelSet;
    }

    public String getFolderName() {
        return folderName;
    }
}
