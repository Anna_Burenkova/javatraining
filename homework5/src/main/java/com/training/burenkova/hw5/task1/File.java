package com.training.burenkova.hw5.task1;

public class File implements FileModel {
    private String fileName;
    private String fileExtension;

    public File(String name) {
        if (name != null) {
            String[] file = name.split("[.]");
            fileName = file[0];
            fileExtension = "." + file[1];
        }

    }

    @Override
    public void print(String tab) {
        System.out.println(tab + "/" + fileName + fileExtension);

    }
}
