package com.training.burenkova.hw5.task1;

import java.util.ArrayList;
import java.util.List;

public class InputDataParsing {

    public List<String> parse(String row) {
        List<String> fileModelList = new ArrayList<>();
        if (row != null) {
            for (String fileModelElement : row.split("/")) {
                fileModelList.add(fileModelElement.trim());
            }
        } else {
            throw new NullPointerException();
        }
        return fileModelList;
    }
}
