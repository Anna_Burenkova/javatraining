package com.training.burenkova.hw5.task1;

import java.util.Iterator;
import java.util.List;

public class FileModelCreating {
    private Folder folderStructure = new Folder();

    public Folder createFileModel(List<String> fileModelList) {
        if (fileModelList != null) {
            Folder folderTemp = folderStructure;
            for (int i = 0; i < fileModelList.size(); i++) {
                if (fileModelList.get(i).contains(".")) {
                    folderTemp.addFile(new File(fileModelList.get(i)));
                } else {
                    Folder folder = new Folder(fileModelList.get(i));
                    if (!folderTemp.addFolder(folder)) {
                        Iterator iterator = folderTemp.getFileModelSet().iterator();
                        while (iterator.hasNext()) {
                            folderTemp = ((Folder) iterator.next());
                            if (folder.getFolderName().equals(folderTemp.getFolderName())) {
                                break;
                            }
                        }
                    } else {
                        folderTemp = folder;
                    }
                }
            }
        } else {
            throw new NullPointerException();
        }

        return folderStructure;

    }
}
