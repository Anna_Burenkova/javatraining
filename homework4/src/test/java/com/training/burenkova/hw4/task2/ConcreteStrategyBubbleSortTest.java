package com.training.burenkova.hw4.task2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class ConcreteStrategyBubbleSortTest {

    Strategy strategyBubbleSort;
    @Before
    public void initTest() {
       strategyBubbleSort=new ConcreteStrategyBubbleSort();
    }

    @After
    public void afterTest() {
    strategyBubbleSort=null;
    }

    @Test
    public void sort() {
        int[] expectedSortedArray = {1, 2, 3};
        int[] sortedArray = strategyBubbleSort.sort(new int[]{2, 3, 1});
        assertArrayEquals(expectedSortedArray, sortedArray);

    }
}