package com.training.burenkova.hw4.task2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ContextTest {
    private Context context;

    @Before
    public void initTest() {
        context = new Context();
    }

    @After
    public void afterTest() {
        context = null;
    }

    @Test
    public void setStrategyBubbleSort() {
        context.setStrategy(new ConcreteStrategyBubbleSort());
        assertEquals(new ConcreteStrategyBubbleSort().getClass(), context.getStrategy().getClass());
    }

    @Test
    public void setStrategySelectionSort() {
        context.setStrategy(new ConcreteStrategySelectionSort());
        assertEquals(new ConcreteStrategySelectionSort().getClass(), context.getStrategy().getClass());
    }

    @Test
    public void sortStrategyBubble() {
        context.setStrategy(new ConcreteStrategyBubbleSort());
        int[] expectedSortedArray = {1, 2, 3};
        int[] sortedArray = context.sortStrategy(new int[]{2, 3, 1});
        assertArrayEquals(expectedSortedArray, sortedArray);
    }

    @Test
    public void sortStrategySelection() {
        context.setStrategy(new ConcreteStrategySelectionSort());
        int[] expectedSortedArray = {1, 2, 3};
        int[] sortedArray = context.sortStrategy(new int[]{2, 3, 1});
        assertArrayEquals(expectedSortedArray, sortedArray);
    }
}