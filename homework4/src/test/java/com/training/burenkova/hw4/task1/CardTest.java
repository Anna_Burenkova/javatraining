package com.training.burenkova.hw4.task1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

/**
 * Test suite for Card class.
 */
public class CardTest {
    private Card card1, card2;

    @Before
    public void initTest() {
        card1 = new Card("Burenkova", new BigDecimal("100.2"));
        card2 = new Card("Burenkova");
    }

    @After
    public void afterTest() {
        card1 = null;
        card2 = null;
    }

    @Test(expected = IllegalArgumentException.class)
    public void Card() {
        Card card = new Card("Burenkova", new BigDecimal(-100));
    }

    @Test
    public void withdrawMoneyFromCard_whenPlusBalance() {
        BigDecimal sum = new BigDecimal("10.1");
        BigDecimal expectedBalanceFromCard1 = new BigDecimal("90.1");
        BigDecimal expectedReturnSum = sum;
        BigDecimal returnSum = card1.withdrawMoneyFromCard(sum);
        BigDecimal balanceFromCard1 = card1.getMoneyFromCard();
        assertEquals(expectedReturnSum, returnSum);
        assertEquals(expectedBalanceFromCard1, balanceFromCard1);

    }

    @Test
    public void withdrawMoneyFromCard_whenZeroBalance() {
        BigDecimal sum = new BigDecimal("10.1");
        BigDecimal expectedBalanceFromCard2 = new BigDecimal("0");
        BigDecimal expectedReturnSum = BigDecimal.ZERO;
        BigDecimal returnSum = card2.withdrawMoneyFromCard(sum);
        BigDecimal balanceFromCard2 = card2.getMoneyFromCard();
        assertEquals(expectedReturnSum, returnSum);
        assertEquals(expectedBalanceFromCard2, balanceFromCard2);
    }

    @Test
    public void addMoneyToCard() {
        BigDecimal sum = new BigDecimal("10.1");
        card1.addMoneyToCard(sum);
        BigDecimal expectedBalanceFromCard1 = new BigDecimal("110.3");
        BigDecimal balanceFromCard1 = card1.getMoneyFromCard();
        assertEquals(expectedBalanceFromCard1, balanceFromCard1);

    }

    @Test
    public void addMoneyToCard_WhenSumNegative() {
        BigDecimal sum = new BigDecimal("-10.1");
        card2.addMoneyToCard(sum);
        BigDecimal expectedBalanceFromCard1 = new BigDecimal("0");
        BigDecimal balanceFromCard1 = card2.getMoneyFromCard();
        assertEquals(expectedBalanceFromCard1, balanceFromCard1);

    }

    @Test
    public void getMoneyFromCard() {
        BigDecimal expectedBalanceFromCard1 = new BigDecimal("100.2");
        BigDecimal balanceFromCard1 = card1.getMoneyFromCard();
        BigDecimal expectedBalanceFromCard2 = new BigDecimal("0");
        BigDecimal balanceFromCard2 = card2.getMoneyFromCard();
        assertEquals(expectedBalanceFromCard1, balanceFromCard1);
        assertEquals(expectedBalanceFromCard2, balanceFromCard2);
    }


    @Test
    public void getBalanceInUsd() {
        BigDecimal expectedBalanceUsd = new BigDecimal(46.092).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        BigDecimal balanceUsd = card1.getBalanceInUsd(0.46).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        assertEquals(expectedBalanceUsd, balanceUsd);

    }

    @Test(expected = IllegalArgumentException.class)
    public void displayBalanceInDifferentCurrencyException() {
        BigDecimal balanceUSD = card1.getBalanceInUsd(-0.46);
    }


}