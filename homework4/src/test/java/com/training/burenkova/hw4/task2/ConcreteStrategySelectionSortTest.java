package com.training.burenkova.hw4.task2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class ConcreteStrategySelectionSortTest {
    Strategy strategySelectionSort;

    @Before
    public void initTest() {
        strategySelectionSort = new ConcreteStrategySelectionSort();
    }

    @After
    public void afterTest() {
        strategySelectionSort = null;
    }

    @Test
    public void sort() {
        int[] expectedSortedArray = {1, 2, 3};
        int[] sortedArray = strategySelectionSort.sort(new int[]{2, 3, 1});
        assertArrayEquals(expectedSortedArray, sortedArray);

    }
}