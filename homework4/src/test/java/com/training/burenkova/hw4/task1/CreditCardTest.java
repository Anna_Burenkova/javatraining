package com.training.burenkova.hw4.task1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class CreditCardTest {
    private Card card;

    @Before
    public void initTest() {
        card = new CreditCard("Burenkova", new BigDecimal("100.2"));
    }

    @After
    public void afterTest() {
        card = null;
    }

    @Test
    public void withdrawMoneyFromCard() {
        BigDecimal sum = new BigDecimal("100.3");
        BigDecimal expectedBalanceFromCard1 = new BigDecimal("-0.1");
        BigDecimal expectedReturnSum = sum;
        BigDecimal returnSum = card.withdrawMoneyFromCard(sum);
        BigDecimal balanceFromCard1 = card.getMoneyFromCard();
        assertEquals(expectedReturnSum, returnSum);
        assertEquals(expectedBalanceFromCard1, balanceFromCard1);

    }

}