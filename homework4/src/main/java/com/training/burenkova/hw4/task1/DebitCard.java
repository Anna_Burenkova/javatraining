package com.training.burenkova.hw4.task1;

import java.math.BigDecimal;

public class DebitCard extends Card {

    public DebitCard(String cardHolderName, BigDecimal accountBalance) {
        super(cardHolderName, accountBalance);
    }

    public DebitCard(String cardHolderName) {
        super(cardHolderName);
    }
}
