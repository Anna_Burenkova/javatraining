package com.training.burenkova.hw4.task1;

import java.math.BigDecimal;

public class CreditCard extends Card {
    public CreditCard(String cardHolderName, BigDecimal accountBalance) {
        super(cardHolderName, accountBalance);
    }

    public CreditCard(String cardHolderName) {
        super(cardHolderName);
    }

    @Override
    public BigDecimal withdrawMoneyFromCard(BigDecimal sum) {
        if (sum.compareTo(BigDecimal.ZERO) == 1) {
            setAccountBalance(getMoneyFromCard().subtract(sum));
        } else {
            sum = BigDecimal.ZERO;
            System.out.println("Wrong sum!");
        }
        return sum;
    }
}
