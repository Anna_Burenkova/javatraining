package com.training.burenkova.hw4.task1;

import java.math.BigDecimal;

/**
 * This class is for card balance operations
 */
public class Card {
    private String cardHolderName;
    private BigDecimal accountBalance;

    public Card(String cardHolderName, BigDecimal accountBalance) {
        if (accountBalance.compareTo(BigDecimal.ZERO) != -1) {
            this.accountBalance = accountBalance;
        } else {
            throw new IllegalArgumentException("Account Balance < 0");
        }
        this.cardHolderName = cardHolderName;

    }

    public Card(String cardHolderName) {
        this.cardHolderName = cardHolderName;
        this.accountBalance = BigDecimal.ZERO;
    }

    /**
     * Method is for getting money from card
     *
     * @param sum - sum for getting
     */
    public BigDecimal withdrawMoneyFromCard(BigDecimal sum) {
        if (sum.compareTo(BigDecimal.ZERO) == 1 && accountBalance.compareTo(sum) == 1) {
            accountBalance = accountBalance.subtract(sum);
        } else {
            sum = BigDecimal.ZERO;
            System.out.println("Wrong sum!");
        }
        return sum;
    }

    /**
     * Method is for adding money to card
     *
     * @param sum - sum for adding
     */
    public void addMoneyToCard(BigDecimal sum) {
        if (sum.compareTo(BigDecimal.ZERO) == 1) {
            accountBalance = accountBalance.add(sum);
        } else {
            System.out.println("Sum <= 0");
        }
    }

    /**
     * Method is for getting account balance
     */
    public BigDecimal getMoneyFromCard() {
        return accountBalance;
    }

    /**
     * Method is for displaying balance in USD
     *
     * @param exchangeRate - exchange rate
     * @return balance in selected currency
     */
    public BigDecimal getBalanceInUsd(double exchangeRate) {
        if (exchangeRate > 0) {
            BigDecimal balanceInUSD;
            balanceInUSD = accountBalance.multiply(BigDecimal.valueOf(exchangeRate));
            return balanceInUSD;
        } else {
            throw new IllegalArgumentException("Exchange rate < 0");
        }
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }
}

