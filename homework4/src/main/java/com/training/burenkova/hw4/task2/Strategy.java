package com.training.burenkova.hw4.task2;

public interface Strategy {
    int[] sort(int[] array);
}
