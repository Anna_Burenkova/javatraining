package com.training.burenkova.hw4.task1;

import java.math.BigDecimal;

public class Atm {
    Card card;

    public Atm(Card card) {
        this.card = card;
    }

    public BigDecimal withdrawMoneyFromCard(BigDecimal sum) {
        return card.withdrawMoneyFromCard(sum);
    }

    public void addMoneyToCard(BigDecimal sum) {
        card.addMoneyToCard(sum);
    }

    public BigDecimal getMoneyFromCard() {
        return card.getMoneyFromCard();
    }

    public BigDecimal getBalanceInUSD(double exchangeRate) {
        return card.getBalanceInUsd(exchangeRate);
    }

}
