package com.training.burenkova.hw4.task2;

public class Context {
    private Strategy strategy;

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public int[] sortStrategy(int[] array) {
        return strategy.sort(array);
    }

    public Strategy getStrategy() {
        return strategy;
    }
}
