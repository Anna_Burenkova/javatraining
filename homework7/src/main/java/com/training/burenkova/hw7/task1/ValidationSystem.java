package com.training.burenkova.hw7.task1;

public class ValidationSystem {
    private static Validator<Object> validator;

    public static void validate(Object value) throws ValidationFailedException {
        if (value instanceof String) {
            validator = new StringValidator();
        } else if (value instanceof Integer) {
            validator = new IntegerValidator();
        } else {
            throw new IllegalArgumentException();
        }
        validator.validate(value);
    }
}
