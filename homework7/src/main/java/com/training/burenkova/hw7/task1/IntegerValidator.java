package com.training.burenkova.hw7.task1;

public class IntegerValidator implements Validator {
    @Override
    public void validate(Object value) throws ValidationFailedException {
        if ((Integer) value >= 1 && (Integer) value <= 10) {
            System.out.println("Integer validation successful");
        } else {
            throw new ValidationFailedException("Integer validation failed");
        }
    }
}
