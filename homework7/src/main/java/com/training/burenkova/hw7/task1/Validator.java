package com.training.burenkova.hw7.task1;

public interface Validator<T> {
    void validate(T value) throws ValidationFailedException;
}
