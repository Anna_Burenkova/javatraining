package com.training.burenkova.hw7.task1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringValidator implements Validator {

    @Override
    public void validate(Object value) throws ValidationFailedException {
        Pattern pattern = Pattern.compile("[A-Z].+");
        Matcher matcher = pattern.matcher(value.toString());
        boolean isFound = matcher.matches();
        if (isFound) {
            System.out.println("String validation successful");
        } else {
            throw new ValidationFailedException("String validation failed");
        }
    }
}
